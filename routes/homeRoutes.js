const homeController = require('../controllers/homeController');
const webMiddleware = require('../middlewares/web.middleware');

const router = require("express").Router();

router.get('/', webMiddleware.verifyWebSession, homeController.home)
router.get('/login', webMiddleware.redirectIfSigned, homeController.login)
router.get('/register', webMiddleware.redirectIfSigned, homeController.register)

module.exports = router;