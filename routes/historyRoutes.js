const historyController = require('../controllers/api/v1/historyController')
const webMiddleware = require('../middlewares/web.middleware')

const router = require("express").Router();


router.get('/history', webMiddleware.verifyWebSession, historyController.index)
router.get('/history/show/:id', webMiddleware.verifyWebSession, historyController.show)

module.exports = router