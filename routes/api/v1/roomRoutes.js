const express = require('express');
const router = express.Router();
const roomController = require('../../../controllers/api/v1/roomController');
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const { verifyJWT } = require('../../../middlewares/api.middleware')

// Create a new room
router.post('/create', jsonParser, verifyJWT, roomController.createRoom);
router.post('/:id', jsonParser, verifyJWT, roomController.roomAction)

module.exports = router;
