const express = require('express');
const router = express.Router();
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json()
const userControllerV1 = require('../../../controllers/api/v1/userController')
const { verifyJWT } = require("../../../middlewares/api.middleware")

router.post('/login', jsonParser, userControllerV1.login)
router.post('/register', jsonParser, userControllerV1.register)
router.get('/verify', jsonParser, verifyJWT, userControllerV1.verify)



module.exports = router