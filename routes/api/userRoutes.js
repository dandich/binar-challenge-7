const express = require('express');
require('express-group-routes')
const bodyParser = require("body-parser");
const router = express.Router();
const userController = require('../../controllers/api/userController');

const { verifyJWT } = require("../../middlewares/api.middleware")

const urlEncoded = bodyParser.urlencoded({ extended: false });

router.post('/login', urlEncoded, userController.login)
router.post('/register', urlEncoded, userController.register);
router.get('/logout', urlEncoded, userController.logout)
router.get('/delete/:id', urlEncoded, userController.delete)


module.exports = router;
