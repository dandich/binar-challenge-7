const userController = require('../controllers/api/userController')
const webMiddleware = require('../middlewares/web.middleware')

const router = require("express").Router();

router.get('/users', webMiddleware.verifyWebSession, userController.index)

module.exports = router