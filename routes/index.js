const router = require("express").Router();
const homeRoutes = require('./homeRoutes');
const historyRoutes = require('./historyRoutes')
const userRoutes = require('./userRoutes')

const apiUserRoutes = require('./api/userRoutes')
const apiRoomRoutes = require('./api/v1/roomRoutes')

const apiV1UserRoutes = require('./api/v1/userRoutes')
const apiV1RoomRoutes = require('./api/v1/roomRoutes')

// MVC
router.use('/', homeRoutes);
router.use('/', historyRoutes)
router.use('/', userRoutes)

// MCR
router.use('/api/user', apiUserRoutes)

// API
router.group('/api/v1', route => {
  route.use('/user', apiV1UserRoutes)
  route.use('/room', apiV1RoomRoutes)
})


module.exports = router; 