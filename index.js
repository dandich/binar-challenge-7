const express = require("express");
const Sequelize = require("sequelize");
const session = require("express-session");
const bodyParser = require('body-parser');
const cookieParser = require("cookie-parser");

const dotenv = require('dotenv');
dotenv.config();
const env = process.env.NODE_ENV || 'development';

const config = require(__dirname + '/config/config.json')[env];
const router = require('./routes')

const SequelizeStore = require("connect-session-sequelize")(session.Store);
const sequelize = new Sequelize(config.database, config.username, config.password, {
    dialect: config.dialect
});


const app = express()
app.use(session({
    secret: "secret",
    store: new SequelizeStore({
        db: sequelize
    }),
    cookie: {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24 * 7 // Set cookie to expire in a week
    },
    saveUninitialized: true,
    resave: false
}));
app.set('view engine', 'ejs');
app.use(express.json());
sequelize.sync();
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(router)

const port = "3000";
app.listen(port, () => {
    console.log(`App listening on port: ${port}`);
})
