'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Room extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Room.belongsTo(models.User, {
        foreignKey: 'creatorId',
        onDelete: 'CASCADE'
      })
      Room.belongsTo(models.User, {
        foreignKey: 'targetId',
        onDelete: 'CASCADE'
      })
      Room.hasMany(models.History, {
        foreignKey: 'roomId',
        onDelete: 'CASCADE'
      })
    }
  }
  Room.init({
    creatorId: DataTypes.INTEGER,
    targetId: DataTypes.INTEGER,
    creatorChoice1: DataTypes.STRING,
    creatorChoice2: DataTypes.STRING,
    creatorChoice3: DataTypes.STRING,
    targetChoice1: DataTypes.STRING,
    targetChoice2: DataTypes.STRING,
    targetChoice3: DataTypes.STRING,
    firstResult: DataTypes.STRING,
    secondResult: DataTypes.STRING,
    thirdResult: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Room',
  });
  return Room;
};