'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      History.belongsTo(models.Room, {
        foreignKey: 'roomId',
        onDelete: 'CASCADE'
      })
      History.belongsTo(models.User, {
        foreignKey: 'creatorId',
        onDelete: 'CASCADE'
      })
      History.belongsTo(models.User, {
        foreignKey: 'targetId',
        onDelete: 'CASCADE'
      })
    }
  }
  History.init({
    roomId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    firstResult: {
      type: DataTypes.STRING
    },
    secondResult: {
      type: DataTypes.STRING
    },
    thirdResult: {
      type: DataTypes.STRING
    }
  }, {
    sequelize,
    modelName: 'History',
  });
  return History;
};