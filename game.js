function RPSgame(creatorChoice, targetChoice) {
  if (creatorChoice === targetChoice) {
    return `it's a Tie!`;
  }

  switch (creatorChoice) {
    case 'rock':
      return targetChoice === 'paper' ? `Target chose ${targetChoice} so, Target wins!` : `Creator chose ${creatorChoice} so, Creator wins!`;
    case 'paper':
      return targetChoice === 'scissors' ? `Target chose ${targetChoice} so, Target wins!` : `Creator chose ${creatorChoice} so, Creator wins!`;
    case 'scissors':
      return targetChoice === 'rock' ? `Target chose ${targetChoice} so, Target wins!` : `Creator chose ${creatorChoice} so, Creator wins!`;
    default:
      return null;
  }
}
module.exports = { RPSgame }