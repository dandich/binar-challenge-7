const { User } = require("../../models");

module.exports = {
  register: async (req, res) => {
    try {
      let user = await User.create({
        username: req.body.username,
        password: req.body.password,
      });
      res.redirect('/login')
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Something went wrong' })
    }
  },
  login: async (req, res) => {
    let message = {
      type: 'error',
      message: 'Login failed, username/password does not match'
    };

    try {
      let user = await User.findOne({
        where: { username: req.body.username }
      });

      // Password not valid
      if (!user.validPassword(req.body.password)) {
        return res.render("login", {
          message: message,
          session: req.session
        })
      }

      // If valid, store to session
      req.session.userId = user.id;
      req.session.username = user.username;
      req.session.isAdmin = user.isAdmin == true;

      res.redirect('/')

    } catch (error) {
      return res.render("login", {
        message: message
      })
    }
  },
  index: async (req, res) => {
    if (req.session.isAdmin) {
      try {
        let users = await User.findAll({
          where: { isAdmin: false }
        });
        res.render("users/index", { users, session: req.session });
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Something went wrong' });
      }
    } else {
      res.status(401).json({ message: 'Unauthorized' });
    }
  },
  delete: async (req, res) => {
    if (req.session.isAdmin) {
      try {
        const userId = req.params.id;
        const user = await User.findByPk(userId);

        if (!user) {
          return res.status(404).json({ message: 'User not found' });
        }

        await user.destroy();
        res.redirect("/users")
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Something went wrong' });
      }
    } else {
      res.status(401).json({ message: 'Unauthorized' });
    }
  },
  logout: async (req, res) => {
    if (req.session.userId) {
      req.session.destroy();
      res.redirect('/')
    }
  }
}