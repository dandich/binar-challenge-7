const { User } = require("../../../models");
const { error, success } = require('../../../utils/response/utils.json')

module.exports = {
  login: async (req, res) => {
    let message = 'Login failed, username/password does not match';

    try {
      let user = await User.findOne({
        where: { username: req.body.username }
      });

      // Password not valid
      if (!user.validPassword(req.body.password)) {
        return error(res, 400, message, {});
      }

      // Login success
      return success(res, 200, 'Login Success', {
        token: user.generateToken()
      })

    } catch (err) {
      return err(res, 400, err.message, {});
    }
  },
  register: async (req, res) => {
    try {
      let user = await User.create({
        username: req.body.username,
        password: req.body.password,
      });

      return success(res, 201, 'user created');
    } catch (err) {
      return error(res, 400, err.message, {});
    }
  },
  verify: async (req, res) => {
    return success(res, 200, 'OK');
  }
};
