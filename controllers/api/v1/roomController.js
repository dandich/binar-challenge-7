const db = require('../../../models');
const { Room } = require('../../../models');
const { History } = require('../../../models')
const { RPSgame } = require('../../../game')

module.exports = {
  createRoom: async (req, res) => {
    const creatorId = req.user.id
    const { targetId } = req.body
    try {
      if (!creatorId) {
        return res.status(401).send({ error: "Unauthorized" })
      }
      let room = await Room.create({ creatorId, targetId });
      res.status(201).json({ room })
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Server error' })
    }
  },
  roomAction: async (req, res) => {
    try {
      const roomId = req.params.id;
      const room = await db.Room.findByPk(roomId);
      if (!room) {
        return res.status(400).send({ error: 'Room not found' })
      }

      // User check allowed or not
      const userId = req.user.id;
      if (room.creatorId === userId) {
        // Creator choices
        room.creatorChoice1 = req.body.creatorChoice1
        room.creatorChoice2 = req.body.creatorChoice2
        room.creatorChoice3 = req.body.creatorChoice3

      } else if (room.targetId === userId) {
        // Target choices
        room.targetChoice1 = req.body.targetChoice1
        room.targetChoice2 = req.body.targetChoice2
        room.targetChoice3 = req.body.targetChoice3

      } else {
        return res.status(403).send({ error: "Not Authorized" })
      }

      const history = await History.findOne({ where: { roomId: room.id } });
      if (!history) {
        const newHistory = await History.create({
          roomId: room.id,
          creatorId: room.creatorId,
          targetId: room.targetId,
          firstResult: null,
          secondResult: null,
          thirdResult: null,
        })
        // Result checker
        if (room.creatorChoice1 && room.targetChoice1 && !room.firstResult) {
          room.firstResult = RPSgame(room.creatorChoice1, room.targetChoice1);
          newHistory.firstResult = RPSgame(room.creatorChoice1, room.targetChoice1);
        }

        if (room.creatorChoice2 && room.targetChoice2 && !room.secondResult) {
          room.secondResult = RPSgame(room.creatorChoice2, room.targetChoice2);
          newHistory.secondResult = RPSgame(room.creatorChoice2, room.targetChoice2);
        }

        if (room.creatorChoice3 && room.targetChoice3 && !room.thirdResult) {
          room.thirdResult = RPSgame(room.creatorChoice3, room.targetChoice3);
          newHistory.thirdResult = RPSgame(room.creatorChoice3, room.targetChoice3);
        }
        await newHistory.save()
      } else {
        if (room.creatorChoice1 && room.targetChoice1) {
          history.firstResult = RPSgame(room.creatorChoice1, room.targetChoice1);
        }
        if (room.creatorChoice2 && room.targetChoice2) {
          history.secondResult = RPSgame(room.creatorChoice2, room.targetChoice2);
        }
        if (room.creatorChoice3 && room.targetChoice3) {
          history.thirdResult = RPSgame(room.creatorChoice3, room.targetChoice3);
        }
        await history.save();

        if (room.creatorChoice1 && room.targetChoice1) {
          room.firstResult = history.firstResult;
        }

        if (room.creatorChoice2 && room.targetChoice2) {
          room.secondResult = history.secondResult;
        }

        if (room.creatorChoice3 && room.targetChoice3) {
          room.thirdResult = history.thirdResult;
        }
      }

      await room.save();
      return res.send(room)
    } catch (error) {
      console.error(error)
      return res.status(500).send({ error: 'Server Error' })
    }
  }
};
