const { Op } = require('sequelize')
const { History } = require('../../../models')
const { Room } = require('../../../models')

module.exports = {
  index: async (req, res) => {
    try {
      let rooms;
      if (req.user.isAdmin) {
        rooms = await Room.findAll({
          include: [History],
        });
      } else {
        rooms = await Room.findAll({
          where: {
            [Op.or]: [
              { creatorId: req.user.id },
              { targetId: req.user.id }
            ]
          },
          include: [History]
        })
      }
      res.render('history', { rooms, session: req.session })
    } catch (err) {
      console.error(err);
      res.status(500).send('Internal Server Error');
    }
  },
  show: async (req, res) => {
    try {
      const roomId = req.params.id;
      const room = await Room.findByPk(roomId, { include: [History] })

      if (!room) {
        return res.status(404).send('Room not found')
      }

      const userIsAuthorized = room.creatorId === req.user.id || room.targetId === req.user.id

      if (!userIsAuthorized && !req.user.isAdmin) {
        return res.status(403).send('Forbidden')
      }

      res.render('history/show', { room, history: room.Histories, session: req.session })
    } catch (err) {
      console.error(err);
      res.status(500).send('Internal Server Error')
    }
  }
}