'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      creatorId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
          as: 'creatorId'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      },
      targetId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'id',
          as: 'targetId'
        },
        onDelete: 'CASCADE',
        onUpdate: 'CASCADE'
      },
      creatorChoice1: {
        type: Sequelize.STRING
      },
      creatorChoice2: {
        type: Sequelize.STRING,
        allowNull: true
      },
      creatorChoice3: {
        type: Sequelize.STRING,
        allowNull: true
      },
      targetChoice1: {
        type: Sequelize.STRING,
        allowNull: true
      },
      targetChoice2: {
        type: Sequelize.STRING,
        allowNull: true
      },
      targetChoice3: {
        type: Sequelize.STRING,
        allowNull: true
      },
      firstResult: {
        type: Sequelize.STRING,
        allowNull: true
      },
      secondResult: {
        type: Sequelize.STRING,
        allowNull: true
      },
      thirdResult: {
        type: Sequelize.STRING,
        allowNull: true
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rooms');
  }
};