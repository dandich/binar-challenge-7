const { User } = require("../models");

module.exports = {
  verifyWebSession: async (req, res, next) => {
    if (!req.session || !req.session.userId) {
      return res.redirect("/login");
    }

    try {
      const user = await User.findByPk(req.session.userId);

      if (!user) {
        return res.redirect("/login");
      }

      // Add the isAdmin
      req.user = user;
      req.session.isAdmin = user.isAdmin;
      next();
    } catch (error) {
      console.error("Error verifying user session:", error);
      return res.redirect("/login");
    }
  },
  redirectIfSigned: async (req, res, next) => {
    if (!req.session || !req.session.userId) {
      return next();
    }

    return res.redirect("/");
  }
};